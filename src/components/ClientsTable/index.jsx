import React from 'react';
import block from 'bem-cn';
import shortid from 'shortid';
import { Icon } from 'react-fa';
import PropTypes from 'prop-types';
import './style.scss';

const b = block('clientsTable');

function ClientsTable({ clients, deleteClient, updateClient }) {
  return (
    <table className={b()}>
      <thead className={b('head')}>
        <tr>
          <th />
          <th>Клиент</th>
          <th>Телефон</th>
          <th>E-mail</th>
          <th>Дата последнего посещения</th>
          <th>Сумма оплат</th>
          <th>Количество посещений</th>
          <th>Активный абонемент</th>
        </tr>
      </thead>
      <tbody className={b('body')}>
        { clients.map((client, idx) =>
          <tr key={shortid.generate()}>
            <td>
              <button className={b('button')} onClick={e => updateClient(e, client, idx)}>
                <Icon name="pencil" />
              </button>
              <button className={b('button')} onClick={e => deleteClient(e, idx)}>
                <Icon name="times" />
              </button>
            </td>
            <td>{ client.name }</td>
            <td>{ client.phone }</td>
            <td>{ client.email }</td>
            <td>-----</td>
            <td>-----</td>
            <td>-----</td>
            <td>-----</td>
          </tr>
        ) }
      </tbody>
    </table>
  );
}

ClientsTable.propTypes = {
  clients: PropTypes.arrayOf(PropTypes.object).isRequired,
  deleteClient: PropTypes.func.isRequired,
  updateClient: PropTypes.func.isRequired
};

export default ClientsTable;
