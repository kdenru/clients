import React, { Component } from 'react';
import block from 'bem-cn';
import { Icon } from 'react-fa';

import './style.scss';

import ClientForm from '../../components/ClientForm';
import ClientsTable from '../../components/ClientsTable';

const b = block('clients');

class Clients extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showForm: false,
      formData: {
        name: '',
        phone: '',
        email: ''
      },
      clients: []
    };

    this.showForm = this.showForm.bind(this);
    this.hideForm = this.hideForm.bind(this);
    this.addClient = this.addClient.bind(this);
    this.updateClient = this.updateClient.bind(this);
    this.deleteClient = this.deleteClient.bind(this);
    this.changeForm = this.changeForm.bind(this);
  }

  showForm(e) {
    e.preventDefault();
    this.setState({ showForm: true });
  }

  hideForm(e) {
    e.preventDefault();
    this.setState({
      showForm: false,
      formData: {
        name: '',
        phone: '',
        email: ''
      },
    });
  }

  addClient(e) {
    e.preventDefault();
    const idx = this.state.formData.idx;
    let clients = this.state.clients;

    if (idx >= 0) {
      clients[idx] = this.state.formData;
    } else {
      clients = [...clients, this.state.formData];
    }

    this.setState({
      clients,
      showForm: false,
      formData: {
        name: '',
        phone: '',
        email: ''
      }
    });
  }

  deleteClient(e, idx) {
    e.preventDefault();
    this.setState({ clients: this.state.clients.filter((_, i) => i !== idx) });
  }

  updateClient(e, client, idx) {
    e.preventDefault();
    this.setState({
      showForm: true,
      formData: {
        ...client,
        idx
      }
    });
  }

  changeForm(e) {
    const value = e.target.value;
    const fieldName = e.target.name;
    switch (fieldName) {
      case 'name': {
        this.setState({ formData: { ...this.state.formData, name: value } });
        break;
      }
      case 'email': {
        this.setState({ formData: { ...this.state.formData, email: value } });
        break;
      }
      case 'phone': {
        this.setState({ formData: { ...this.state.formData, phone: value } });
        break;
      }
      default: break;
    }
  }

  render() {
    const { clients, showForm, formData } = this.state;
    return (
      <div className={b()}>
        <h2 className={b('title')}>Клиенты</h2>
        <button className={b('button')} onClick={this.showForm}>
          <Icon className={b('icon')()} name="plus-circle" />
          Добавить клиента
        </button>
        { showForm ?
          <ClientForm
            onDataChange={this.changeForm}
            hideForm={this.hideForm}
            save={this.addClient}
            formData={formData}
          />
          :
          null
        }
        <ClientsTable
          clients={clients}
          deleteClient={this.deleteClient}
          updateClient={this.updateClient}
        />
      </div>
    );
  }
}

export default Clients;
