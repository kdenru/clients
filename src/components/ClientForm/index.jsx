import React from 'react';
import block from 'bem-cn';
import { Icon } from 'react-fa';
import PropTypes from 'prop-types';

import './style.scss';

const b = block('clientForm');

function ClientForm({ hideForm, formData, onDataChange, save }) {
  return (
    <form className={b()}>
      <div className={b('field')}>
        <label htmlFor="name">Имя</label>
        <input id="name" type="text" name="name" value={formData.name} onChange={onDataChange} />
      </div>
      <div className={b('field')}>
        <label htmlFor="phone">Телефон</label>
        <input id="phone" type="text" name="phone" value={formData.phone} onChange={onDataChange} />
      </div>
      <div className={b('field')}>
        <label htmlFor="email">E-mail</label>
        <input id="email" type="email" name="email" value={formData.email} onChange={onDataChange} />
      </div>
      <button type="submit" className={b('saveButton')} onClick={save}>
        Сохранить
      </button>
      <button className={b('rollupButton')} onClick={hideForm}>
        <Icon name="angle-up" />
      </button>
      <button className={b('rollupButton', { right: true })} onClick={hideForm}>
        <Icon name="angle-up" />
      </button>
    </form>
  );
}

ClientForm.propTypes = {
  save: PropTypes.func.isRequired,
  hideForm: PropTypes.func.isRequired,
  onDataChange: PropTypes.func.isRequired,
  formData: PropTypes.objectOf(PropTypes.any).isRequired
};

export default ClientForm;
