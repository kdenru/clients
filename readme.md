### setting up

Copy repo to local machine:
```
git clone https://gitlab.com/kdenru/clients.git
```
Move to project directory:
```
cd clients
```
Install dependencies:
```
npm install
```
Run:
```
npm start
```
This command runs json-server at localhost:3000.