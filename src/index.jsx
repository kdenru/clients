import React from 'react';
import ReactDOM from 'react-dom';
import './style.scss';

import Clients from './containers/Clients';

if (module.hot) {
  module.hot.accept();
}

ReactDOM.render(
  <Clients />,
  document.getElementById('root')
);
