const path = require('path');
const express = require('express');
const webpack = require('webpack');
const winston = require('winston');
const webpackDevMiddleware = require("webpack-dev-middleware");
const webpackHotMiddleware = require("webpack-hot-middleware");
const webpackConfig = require('./webpack.config.js');

const app = express();

const compiler = webpack(webpackConfig);

app.use(webpackDevMiddleware(compiler, {
  hot: true,
  filename: 'bundle.js',
  publicPath: '/dist',
  stats: {
    colors: true,
  },
  historyApiFallback: true,
}));

app.use(webpackHotMiddleware(compiler, {
  log: winston.info,
  path: '/__webpack_hmr',
  heartbeat: 10 * 1000,
}));

app.use(express.static(`${__dirname}/dist`));

app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, 'index.html'));
});


const server = app.listen(3000, 'localhost', () => {
  const host = server.address().address;
  const port = server.address().port;
  winston.info('Example app listening at http://%s:%s', host, port);
});
